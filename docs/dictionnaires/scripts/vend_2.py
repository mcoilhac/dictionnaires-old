def vend_encore(ferme):
    ...

ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}
vend_encore(ferme_gaston)

# Tests
assert ferme_gaston == {"lapin": 4, "vache": 6, "cheval": 3}

