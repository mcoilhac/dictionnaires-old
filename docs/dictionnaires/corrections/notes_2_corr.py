def bulletin(les_notes) -> list:
    liste_notes = []
    for devoir in les_notes:
        liste_notes.append((devoir, les_notes[devoir]))
    return liste_notes


notes_Alice = {"test_1": 14, "test_2": 16, "test_3": 18}
print(bulletin(notes_Alice))
