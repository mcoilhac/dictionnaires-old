def nbre_recettes(recettes, ingredient):
    total = 0
    for gateau in recettes:
        if ingredient in recettes[gateau]:
            total = total + 1
    return total

mes_recettes = {"Gâteau au chocolat": ["chocolat", "oeuf", "farine", "sucre"," beurre"],
"Gâteau au yaourt":["yaourt", "oeuf", "farine", "sucre"],
"Crêpes":["oeuf", "farine", "lait", "bière"],
"Quatre-quarts":["oeuf", "farine", "beurre", "sucre"],
"Kouign amann":["farine", "beurre", "sucre"]}

assert nbre_recettes(mes_recettes, "sucre") == 4