---
author: Mireille Coilhac
title:  👏 Crédits
---

* Ce site a été réalisé par Mireille Coilhac, avec l'aide de Charles Poulmaire, Jean-Louis Thirot et des professeurs du groupe e-nsi.

* L'ensemble des documents sont sous licence [CC-BY-NC-SA 4.0 (Attribution, Utilisation Non Commerciale, ShareAlike)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

* Le site est hébergé par  [la forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/){:target="_blank" }.

* Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/), et surtout [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/) pour la partie Python.

😀 Un grand merci à Frédéric Zinelli, et Vincent-Xavier Jumel qui ont réalisé la partie technique de ce site.


* Le logo :material-book-alphabet: fait partie de mkdocs sous la référence `book-alphabet`
