Le rendu se trouve ici :  https://mcoilhac.forge.apps.education.fr/dictionnaires-old/

Lorsqu'il est écrit  : ⌛ Une correction viendra plus tard ...   cela signifie que la correction est effectivement présente dans le document 
markdown. Il suffit d'enlever les caractères qui mettent en commentaire la solution (entre `<!---` et `-->`) pour la dévoiler. 
Vérifier l'indentation, car les commentaires ont été sortis des admonitions.
